# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table answer (
  id                            bigint auto_increment not null,
  question_id                   bigint not null,
  value                         varchar(255),
  constraint pk_answer primary key (id)
);

create table question (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  type                          integer,
  questionnaire_id              bigint,
  constraint pk_question primary key (id)
);

create table questionnaire (
  id                            bigint auto_increment not null,
  title                         varchar(255),
  owner_id                      bigint,
  is_active                     tinyint(1) default 0,
  constraint pk_questionnaire primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  username                      varchar(255),
  password                      varchar(255),
  constraint pk_user primary key (id)
);

alter table answer add constraint fk_answer_question_id foreign key (question_id) references question (id) on delete restrict on update restrict;
create index ix_answer_question_id on answer (question_id);

alter table question add constraint fk_question_questionnaire_id foreign key (questionnaire_id) references questionnaire (id) on delete restrict on update restrict;
create index ix_question_questionnaire_id on question (questionnaire_id);

alter table questionnaire add constraint fk_questionnaire_owner_id foreign key (owner_id) references user (id) on delete restrict on update restrict;
create index ix_questionnaire_owner_id on questionnaire (owner_id);


# --- !Downs

alter table answer drop foreign key fk_answer_question_id;
drop index ix_answer_question_id on answer;

alter table question drop foreign key fk_question_questionnaire_id;
drop index ix_question_questionnaire_id on question;

alter table questionnaire drop foreign key fk_questionnaire_owner_id;
drop index ix_questionnaire_owner_id on questionnaire;

drop table if exists answer;

drop table if exists question;

drop table if exists questionnaire;

drop table if exists user;

