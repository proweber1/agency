package controllers;

import com.google.inject.Inject;
import forms.LoginForm;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserProviderInterface;
import views.html.*;

import java.util.Optional;

/**
 * Контроллер для автризации пользователей
 *
 * @author proweber1
 */
public class AuthController extends Controller {

    private final static String LOGIN_ERROR_KEY = "loginError";
    private final static String USERNAME_SESSION_KEY = "username";

    @Inject
    private FormFactory formFactory;

    @Inject
    private UserProviderInterface userProvider;

    /**
     * Открывает страницу авторизации, если пользователь авторизирован,
     * то его сразу перекинет на dashboard в системе, этот экшен
     * реагирует только на <code>GET</code> запрос.
     *
     * @return Страница авторизации
     */
    public Result auth() {
        if (session().containsKey(USERNAME_SESSION_KEY)) {
            return redirect(routes.DashboardController.index());
        }

        return ok(login.render(
                formFactory.form(LoginForm.class)
        ));
    }

    /**
     * Этот метод помогает нам разлогиниться из системы, здесь
     * все очень примитивно, реагирует на <code>GET</code>, удаляет
     * из сессии ключ пользователя на который реагирует {@link play.mvc.Security.Authenticated}
     * и делает редирект на страницу входа.
     *
     * @return Редирект на страницу входа
     */
    public Result logout() {
        session().remove(USERNAME_SESSION_KEY);

        return redirect(routes.AuthController.auth());
    }

    /**
     * Процесс авторизации который может закончится ошибками. Этот метод
     * реагирует на <code>POST</code> и пытается авторизировать
     * пользователя, ищет пользователя, добавляет его в сессию и редиректит
     * на страницу дашборда админки.
     *
     * @return Редирект на дашборд или страницу авторизации с ошибкой
     */
    public Result processAuth() {
        Form<LoginForm> form = formFactory.form(LoginForm.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(login.render(form));
        }

        return loginAttempt(form.get());
    }

    /**
     * Пытается авторизировать пользователя, ищет пользователя по
     * логину и паролю и если не находит, то кидает на страницу входа с flash
     * сообщением об ошибке, если находит, то кидает на страницу дашборда.
     *
     * @param loginForm Форма которая содержит в себе запрос на авторизацию
     * @return Редирект либо на страницу входа, либо в админку
     */
    private Result loginAttempt(LoginForm loginForm) {
        Optional<User> userOptional = userProvider.findUserForLogin(loginForm);
        if (userOptional.isPresent()) {
            session(USERNAME_SESSION_KEY, userOptional.get().getUsername());
            return redirect(routes.DashboardController.index());
        }

        flash(LOGIN_ERROR_KEY, "Неверный логин или пароль");
        return redirect(routes.AuthController.auth());
    }

}
