package controllers;

import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import models.Answer;
import models.Question;
import models.Questionnaire;
import models.User;
import play.libs.Json;
import play.mvc.*;

import play.twirl.api.Html;
import services.QuestionnaireServiceInterface;
import views.html.system.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Security.Authenticated
public class QuestionnaireController extends Controller {

    @Inject
    private QuestionnaireServiceInterface questionnaireService;

    public final static String SHOW_QUESTIONNAIRE_ACTION = "show";
    public final static String PASS_QUESTIONNAIRE_ACTION = "pass";

    /**
     * Загружает из базы данных список исследований и возвращает
     * их на страницу которая их затем отображает.
     *
     * @return Список исследований
     */
    public Result index() {
        List<Questionnaire> questionnaires = questionnaireService.findActiveQuestionnaires();

        return ok(questionnaireList.render(questionnaires));
    }

    /**
     * Позволяет открыть определенное исследование.
     *
     * @param id ID исследования
     * @param action Дейтсвие которое надо сделать с опросником
     * @return представление исследования
     */
    public Result show(long id, String action) {
        Optional<Questionnaire> questionnaire = questionnaireService.findById(id);
        if (!questionnaire.isPresent()) {
            return notFound();
        }

        Questionnaire questionnaireModel = questionnaire.get();

        return ok(
                SHOW_QUESTIONNAIRE_ACTION.equals(action)
                        ? showQuestionnaire.render(questionnaireModel)
                        : passQuestionnaire.render(questionnaireModel)
        );
    }

    /**
     * Этот экшен возвращает страничку на которой можно создать
     * новое исследование
     *
     * @return страничка
     */
    public Result create() {
        return ok(newQuestionnaire.render());
    }

    /**
     * Создает новое исследование посредством ajax запроса.
     *
     * @return Результат выполнения
     */
    @Transactional
    public Result ajaxCreate() {
        JsonNode request = request().body().asJson();

        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setTitle(request.get("title").asText());
        questionnaire.setActive(request.get("isActive").asBoolean());
        questionnaire.setOwner(currentUser());

        List<Question> questions = new ArrayList<>();

        request.get("questions").forEach(node -> {
            Question question = new Question();
            question.setName(node.get("title").asText());
            question.setType(node.get("type").asInt());

            List<Answer> answers = new ArrayList<>();

            node.get("answers").forEach(answerNode -> {
                Answer answer = new Answer();
                answer.setValue(answerNode.asText());

                answers.add(answer);
            });

            if (!answers.isEmpty()) {
                question.setAnswers(answers);
            }

            questions.add(question);
        });

        questionnaire.setQuestions(questions);
        questionnaire.save();

        return ok(Json.toJson(questionnaire));
    }

    private User currentUser() {
        return User.find()
                .where()
                .eq("username", request().username())
                .findUnique();
    }

}
