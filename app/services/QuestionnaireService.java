package services;

import models.Questionnaire;

import java.util.List;
import java.util.Optional;

public class QuestionnaireService implements QuestionnaireServiceInterface {
    @Override
    public List<Questionnaire> findActiveQuestionnaires() {
        return Questionnaire.find()
                .where()
                .eq("isActive", true)
                .findList();
    }

    /**
     * @param id ID исследование
     * @return Опционал исследования
     */
    @Override
    public Optional<Questionnaire> findById(long id) {
        return Optional.ofNullable(
                Questionnaire.find().byId(id)
        );
    }
}
