package services;

import models.Questionnaire;

import java.util.List;
import java.util.Optional;

public interface QuestionnaireServiceInterface {
    List<Questionnaire> findActiveQuestionnaires();

    Optional<Questionnaire> findById(long id);
}
