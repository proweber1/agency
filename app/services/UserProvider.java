package services;

import forms.LoginForm;
import models.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Optional;

public class UserProvider implements UserProviderInterface {
    @Override
    public Optional<User> findUserForLogin(LoginForm loginForm) {
        User user = User.find()
                .where()
                .eq("username", loginForm.getUsername())
                .eq("password", DigestUtils.sha256Hex(loginForm.getPassword()))
                .findUnique();

        return Optional.ofNullable(user);
    }
}
