package services;

import forms.LoginForm;
import models.User;

import java.util.Optional;

public interface UserProviderInterface {
    Optional<User> findUserForLogin(LoginForm loginForm);
}
