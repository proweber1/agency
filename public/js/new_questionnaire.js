var questionsModule = (function questionsModule() {
    'use strict';

    // Transfering data
    var data = {}

    // Elements
    var els = {
        tmplQuestion: document.querySelector('#template_question'),
        tmplAnswers: document.querySelector('#template_answers'),
        tmplAnswer: document.querySelector('#template_answer'),
        addButton: document.querySelector('#form_add_question'),
        sendButton: document.querySelector('#form_send'),
        questions: document.querySelector('#questions'),
        main: document.querySelector('#form_main')
    }

    var removeQuestion = function(element) {
        return function(e) {
            e.preventDefault()
            element.remove()
        }
    }

    var changeSelect = function(element, lastId) {
        return function(e) {
            var isWithAnswer = e.target.value == 3 || e.target.value == 4
            element.innerHTML = isWithAnswer ? _.template(els.tmplAnswers.text)({ id: lastId }) : ''
            if (isWithAnswer) {
                element.querySelector('[data-add-answer]').addEventListener('click', function(e) {
                    e.preventDefault()

                    var answerEl = element.querySelector('[data-add-answer]').appendChild(document.createElement('div'))
                    answerEl.innerHTML = _.template(els.tmplAnswer.text)({ answerText: element.querySelector('input').value })
                    element.querySelector('[data-answers]').appendChild(answerEl)
                    answerEl.querySelector('[data-remove-answer]').addEventListener('click', function(e) {
                        e.preventDefault()
                        answerEl.remove()
                    })
                    element.querySelector('input').value = ''
                })
            }
        }
    }

    var addQuestion = (function() {
        var lastId = 0;

        return function(e) {
            e.preventDefault()

            var div = document.createElement('div')

            div.innerHTML = _.template(els.tmplQuestion.text)({ id: lastId })
            var answerDiv = div.querySelector('[data-answer-div]')

            els.questions.appendChild(div)

            div.querySelector('[data-answer]')
                .addEventListener('change', changeSelect(answerDiv, lastId))

            div.querySelector('[data-remove]')
                .addEventListener('click', removeQuestion(div))

            lastId += 1
        }
    }())

    var serializeForm = function(form) {
        var data = {}
        var a = [].slice.call(form.querySelectorAll('[name]'))
            .map(function(el) {
                if (el.type)
                    if (el.type == 'checkbox') {
                        return [el.name, el.checked]
                    }
                return [el.name, el.value]
            })

        a.forEach(function(pair) {
            if (~pair[0].indexOf('[')) {
                let nspace = pair[0].substr(0, pair[0].indexOf('[')),
                    nvalue = pair[0].slice(pair[0].indexOf('[') + 1, -1)

                if (!data[nspace]) data[nspace] = [{answers: []}]

                if (nvalue === 'answers') {
                    data[nspace][data[nspace].length - 1].answers.push(pair[1])
                } else {
                    if (nvalue in data[nspace][data[nspace].length - 1]) {
                        data[nspace].push({answers: []})
                    }

                    data[nspace][data[nspace].length - 1][nvalue] = pair[1]
                }
            } else {
                data[pair[0]] = pair[1]
            }
        })

        return data
    }

    var normalizeData = function(data) {
        data.isActive = data.isActive == 1
        if (!data.questions) return null
        data.questions.map(function(d) {
            if (Object.prototype.toString.call(d.answers) !== '[object Array]') {
                d.answers = [d.answers]
            }
            return d
        })
        return data
    }

    var getData = function(e) {
        e && e.preventDefault()

        var data = normalizeData(serializeForm(els.main))

        return data
    }

    els.addButton.addEventListener('click', addQuestion)
    els.main.addEventListener('submit', function (e) {
        e && e.preventDefault();

        $.ajax({
            method: 'POST',
            url: els.main.action,
            data: JSON.stringify(getData(e)),
            contentType: 'application/json; charset=utf-8',
            success: function(r) {
                alert('Исследование успешно создано!');
                console.log(r);

                location.href = '/questionnaire/' + r.id + '/show';
            },
            // error: alert('fail'),
            processData: false
        });
    })
}())